---
title: "About/GDPR"
layout: single
excerpt: "Information about this site, GDPR/DSGVO compliance information"
author_profile: true
search: false
permalink: /about/
header:
  image: /images/mh/mh009.jpg
---

## About

This is a private blog/website where I post things I care about, explain stuff I think is worth knowing about. This blog thus solely expresses my private, personal opinions and is not representative of anyone else. 

## Impressum

**Angaben gemäß § 5 TMG  
Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV**

Jan Wildeboer  
Heilwigstraße 91  
81827 München  
GERMANY

### Contact:

E-Mail: blog@tcpid.org

### Data Protection Policy (GDPR)

This website does not collect, store or process Personally Identifiable Information (PII). This website does not require the use of cookies.

### Third Party Inclusion
This website is hosted by [codeberg](https://www.codeberg.org) using [codeberg Pages](https://docs.codeberg.org/codeberg-pages/). The codeberg Privacy Practices are available [here](https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md)
